//
//  objfunction.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

#include "objfunction.hpp"
#include <cmath>
#include <reversead.hpp>

const double A = 10.0;
ReverseAD::adouble funct(const std::vector<ReverseAD::adouble>& x) {
    
    ReverseAD::adouble y;
    // sphere centered at x = 0 //
    //for (int i = 0; i < x.size(); ++i) {y = y + x[i]*x[i];}
    
    // y = x1^4 + x2^4 + .. //
    //for (int i = 0; i < x.size(); ++i) {y = y + pow(x[i],4.0);}
    
    // y = x1^2 + x2^3  //
    //for (int i = 0; i < x.size(); ++i) {y = y + pow(x[i],2 + i);}
    //y = 4*pow(x[0],2) + 9*pow(x[1],2) + 8*x[0] - 36*x[1] + 24;
    //y = pow(x[0],3)/3 + pow(x[1],2) + 2*x[0]*x[1] - 6*x[0] - 3*x[1] + 4;
    y = pow(x[0],2)/2 + x[0]*cos(x[1]);
    
    // Generalized Rosenbrock function //
    //for (int i = 0; i < x.size()-1; ++i) {y = y + 100*pow(x[i+1] - x[i]*x[i],2) + pow(1 - x[i],2);}
    
    // Rastrigin function //
    //for (int i = 0; i < x.size(); ++i) {y = y + (x[i]*x[i] - A*cos(2*M_PI*x[i]) + A);}
    
    // Styblinski-Tang function //
    //for (int i = 0; i < x.size(); ++i){y = y + 0.5*(pow(x[i],4.0) - 16*x[i]*x[i] + 5*x[i]);}
    
    // NEW //
    // Diagonal 1 function //
    //for (int i = 0; i < x.size(); ++i) {y = y + exp(x[i]) - i*x[i];}
    
    // Himmelh function //
    //for (int i = 0; i < x.size()/2 - 1; ++i) {y = y - 3*x[2*i] - 2*x[2*i+1] + 2 + pow(x[2*i],3.0) + pow(x[2*i+1],2.0);}
    
    // Generalized Tridiagonal 1 function //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + pow(x[i] + x[i+1] - 3,2.0) + pow(x[i] - x[i+1] + 1,4.0);}
    
    // Generalized White & Holst function //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + 100*pow(x[i+1] - pow(x[i],3.0),2.0) + pow(1-x[i],2.0);}
    
    // Extended Himmelblau function //
    //for (int i = 0; i < x.size()/2 - 1; ++i) {y = y + pow(x[2*i]*x[2*i] + x[2*i+1] - 11,2.0) + pow(x[2*i] + x[2*i+1]*x[2*i+1] - 7,2.0);}
    
    // Extended Block Diagonal 1 function //
    //for (int i = 0; i < x.size()/2 - 1; ++i) {y = y + pow(pow(x[2*i],2) + pow(x[2*i+1],2) - 2,2) + pow(exp(x[2*i] - 1) - x[2*i+1],2);}
    
    // Extended Maratos function //
    //for (int i = 0; i < x.size()/2 - 1; ++i) {y = y + x[2*i] + 100*pow(pow(x[2*i],2) + pow(x[2*i+1],2) - 1,2);}
    
    // Extended Tridiagonal 2 function //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + pow(x[i]*x[i+1] - 1,2) + 0.1*(x[i] + 1)*(x[i+1] + 1);}
    
    // COSINE function (CUTE) //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + cos(-0.5*x[i+1] + x[i]*x[i]);}
    
    // EG2 function (CUTE) //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + sin(x[0] + x[i]*x[i] - 1);}
    //y = y + 0.5*sin(x[x.size()-1]*x[x.size()-1]);
    
    // Extended Rosenbrock function //
    //for (int i = 0; i < x.size()/2; ++i) {y = y + 100*pow(x[2*i+1] - pow(x[2*i],2),2) + pow(1-x[2*i],2);}
    
    // Diagonal Quadratic //
    //for (int i = 0; i < x.size() - 2; ++i) {y = y + 100*(x[i+1]*x[i+1] + x[i+2]*x[i+2]) + x[i]*x[i];}
    
    // Fletcher's 2nd problem in Numerical Analysis report NA/145, Univ. of Dundee 1992 //
    //for (int i = 0; i < x.size() - 1; ++i) {y = y + 100*pow(x[i+1] - x[i] + 1 - x[i]*x[i],2.0);}
    
    return y;
}

ReverseAD::adouble*
    vector_function(const std::vector<ReverseAD::adouble>& x,
                    int n)
{
    ReverseAD::adouble* F = new ReverseAD::adouble[n];
    
    return F;
}