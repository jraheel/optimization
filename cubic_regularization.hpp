//
//  cubic_regularization.h
//  optim
//
//  Created by Javad Ahmed Raheel on 08/30/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//
//  This code is based on implementation of cubic regularization of
//  Newton's method as described by Nestrov and Polyak in their 2006
//  paper. This has been adapted from the implementation by Corinne Jones,
//  cjones6@uw.edu.


#ifndef cubic_regularization_hpp
#define cubic_regularization_hpp
#include <Eigen/Eigenvalues>
#include <reversead.hpp>
#include "objfunction.hpp"

class CubicRegularization {
public:
    adouble f;                  // the function value at current input variables
    double* xold;               // independent variables at previous point
    double* xnew;               // independent variables at current point
    int n;                      // number of dimensions (independent variables)
    double tolerance;           // tolerance value for the convergence
    double L;                   // Lipschitz constant on the Hessian
    double L0;                  // starting point for line search for M
    double kappa;               // the tolerance for the cubic subproblem
    std::shared_ptr<ReverseAD::TrivialTrace<double>> trace; // for derivaties
    std::shared_ptr<Eigen::SparseVector<double>> grad;
    std::shared_ptr<Eigen::SparseMatrix<double>> Hessian;
    unsigned long iters;        // # of iterations it took to compute cubic regularization
    double Mk;
    double lambda_nplus;
    double lambda_const;
    double lambda_n;
    bool s_success;             // to check if H_labda*s = -grad was solved successfully
    CubicRegularization(double* inits,
                        int dim,
                        double L,
                        double L0,
                        double kappa);      // constructor
    ~CubicRegularization()
    {
        delete [] xold; delete [] xnew;
    }
    void evalDerivatives();
    void computeCubicRegularization();
    void compute_x_new();
    Eigen::VectorXd compute_s(double lambduh);
    Eigen::VectorXd computeStep();
    void compute_lambda_nplus();
};

#endif /* cubic_regularization_h */
