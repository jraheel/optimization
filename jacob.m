function J = jacob(x, t, s)
    gamma = 0.1;
    beta = 1;
    mu = 2*gamma/beta;
    p = phi(t, s);
    theta_star = [1 -2 5]';
    Z = zeros(3);
    X = gamma*p*p';
    N = 1 + mu*p'*p;
    B = beta*N*eye(3);
    J = [Z -X;B -B];
end

