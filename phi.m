function p = phi(t, s)
    if (s == 1)
        if (t <= 25)
            p = [1,1,1]';
        else
            p = [2, -1, -2]';
        end
    else
        p = [1, 1+3*sin(t), 1 + 3*cos(t)]';
    end
end

