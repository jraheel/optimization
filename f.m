function y = f(t,x, phi)
    y = zeros(size(x,1),1);
    gamma = 0.1;
    beta = 1;
    mu = 2*gamma/beta;
    theta_star = [1 -2 5]';
    Z = zeros(3);
    X = gamma*phi*phi';
    N = 1 + mu*phi'*phi;
    B = beta*N*eye(3);
    A = [Z -X;B -B];
    F = [gamma*phi*phi'*theta_star; zeros(3,1)];
    y = A*x + F;
end

