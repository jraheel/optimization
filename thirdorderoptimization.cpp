//
//  thirdorderoptimization.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 2/13/19.
//  Copyright © 2019 Javad Ahmed Raheel. All rights reserved.
//

#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <stdio.h>
#include <iomanip>
#include <algorithm>
#include <chrono>
#include <random>

#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/Eigenvalues>

#include "tensors.hpp"
#include "cubic_regularization.hpp"
#include "thirdorderoptimization.hpp"

using namespace std;
using namespace Eigen;

////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function computeCompetitiveSubspace
 Implements algorithm 4 from Anandkumar and Ge's paper to computes the competitive subspace
 Inputs:
 - H            : pointer to Hessian of the function
 - T            : pointer to the third derivative tensor of the function
 - S            : pointer to the competitive subspace matrix. This shall be changed in place
 - approxRatio  : approximation ratio (Q)
 - L            : Lipschitz bound of the Hessian H
 Outputs:
 - the Frobenius norm of the projected T onto S. If the return value is zero, then S is set to
 NULL, signifying no competitive subspace exists
 */
////////////////////////////////////////////////////////////////////////////////////////////////

double computeCompetetiveSubspace(std::shared_ptr<Eigen::SparseMatrix<double>> H,
                                  std::shared_ptr<Eigen::Tensor<double,3>> T,
                                  shared_ptr<MatrixXd> S,
                                  double approxRatio,
                                  double L)
{
    SelfAdjointEigenSolver<MatrixXd> es;
    es.compute(MatrixXd(*H));
    VectorXd eigVals = es.eigenvalues();
    MatrixXd eigVecs = es.eigenvectors();
    //cout << "eigenvalue decomposition computed\n";
    for (int i = 0; i < (int)H->cols(); ++i) {
        auto Q = eigVecs.block(0,i,H->rows(),H->cols()-i);  // take ith to nth eigenvectors of H
        *S = Q*Q.transpose();                   // S is the projection matrix for column space of Q
        auto projected_T = projectTensor(T, S);             // project T onto S
        //cout << "projection computed\n";
        double Cq = frobeniusNorm(projected_T);             // compute Frobenius norm of projected T
        if (Cq*Cq/(12*L*approxRatio*approxRatio) >= eigVals(eigVals.rows() - i - 1))
            return Cq;
    }
    // have not found a suitable competitve subspace, need to reset S to NULL //
    S.reset();
    S = NULL;
    //cout << "no competitive subspace exists\n";
    return 0;
}


////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function approximateTensorNorm
 Implements algorithm 3 from Anandkumar and Ge's paper.
 Computes a unit vector,u, in a competitive subspace such that for a mode three tensor
 T, T(u,u,u) >= the Frobenius norm of the projection of T onto the subspace defined by S
 Inputs:
 - T: pointer to the third derivative tensor of a function
 - S: pointer to the competitive subspace matrix.
 - Q: a double which is a universal constant = 8/epsilon * (n^1.5)
 Outputs:
 - u: pointer to the unit vector such that T(u,u,u) >= Frobenius norm of T projected onto S
 */
////////////////////////////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::VectorXd>
        approximateTensorNorm(std::shared_ptr<Eigen::Tensor<double,3>> T,
                              std::shared_ptr<Eigen::MatrixXd> S,
                              double Q)
{
    const int iters_standard_gaussian = 1000;
    int n = (int)S->cols();
    std::shared_ptr<Eigen::VectorXd> u(new Eigen::VectorXd(n)); u->setZero();
    std::shared_ptr<Eigen::Tensor<double,3>> projected_T(new Eigen::Tensor<double,3>
                                                       (T->dimension(0),
                                                        T->dimension(1),
                                                        T->dimension(2)));
    // the tolerance to which a standard Gaussian vector in subspace S is computed //
    const double tolerance = 1e-4;
    std::default_random_engine generator;
    std::normal_distribution<double> distribution(0.0,1.0);
    do {
        u->setZero();                                           // initialize to zero vector
        for (int k = 0; k < iters_standard_gaussian; ++k) {     // find a standard gaussian in S
            for (int i = 0; i < S->cols(); ++i)
                (*u)(i) = distribution(generator);              // u is a standard gaussian
            Eigen::VectorXd vec = S->fullPivHouseholderQr().solve(*u);
            if (((*S)*vec - (*u)).norm() < tolerance) {
                break;
            }
        }
        u->normalize();                                         // set u to be a unit vector
        projected_T = projectTensor(T, S);                      // project T onto S
    }     while (abs(tensorDot(projected_T, u)) >=
                 frobeniusNorm(projected_T)/Q);
    if (tensorDot(T, u) < 0)
        *u = -(*u);
    
    /*
    /////////////////////////////////////////////////////////////////
    // ensuring the vector u is in the space spanned by S ///////////
    Eigen::VectorXd vec = S->fullPivHouseholderQr().solve(*u);
    cout << ((*S)*vec - (*u)).norm() << endl;
    // ensuring |T(u,u,u)| >= Frobenius_norm(Projection of T onto S)
    cout << *T << endl;
    cout << frobeniusNorm(T) << endl << endl;
    
    //(*u)(0) = 2;
    //(*u)(1) = 3;
    cout << *u << endl << endl;
    cout << tensorDot(T,u) << endl;
    cout << frobeniusNorm(projectTensor(T, S)) << endl;
    /////////////////////////////////////////////////////////////////
    */
    return u;
}


////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function computeThirdOrderMinimimum
 Implements algorithm 2 from Anandkumar and Ge's paper.
 Computes a third order minimimum of a given function
 Inputs:
 - n   : dimension of the function to be minimized f:R^n ---> R
 - init: pointer to the vector of initial initial guess
 Outputs:
 - third_ord_min: a vector of doubles, storing the values of independent variables
 */
////////////////////////////////////////////////////////////////////////////////////////////////
vector<double> computeThirdOrderMinimum(int n, double * init)
{
    vector<double> third_ord_min(n);
    for (int i = 0; i < n; ++i)
        third_ord_min[i] = init[i];
    std::shared_ptr<TrivialTrace<double>> trace = computeTrace(init, n);
    // have to decide a suitable L, L0, and kappa as the last
    //      three arguments of the cubic regularizer
    CubicRegularization z(init,n,100,1.0,1e-5);
    int t = 5;
    double eps_1 = 0.0;
    for (int i = 0; i < t; ++i) {
        for (int j = 0; j < n; ++j)
            z.xold[j] = third_ord_min[j];
        z.computeCubicRegularization();
        trace = ReverseAD::BaseFunctionReplay::replay_forward(trace, z.xnew, n);
        std::unique_ptr<ReverseAD::BaseReverseThird<double>> derivs
                                        (new ReverseAD::BaseReverseThird<double>(trace));
        std::shared_ptr<DerivativeTensor<size_t, double>> tensor = derivs->compute(n,1);
        std::shared_ptr<SparseVector<double>> grad = computeGrad(tensor,n);
        std::shared_ptr<SparseMatrix<double>> H = computeHessian(tensor,n);
        std::shared_ptr<Eigen::Tensor<double,3>> T = computeThirdDerivative(tensor, n);
        shared_ptr<MatrixXd> S(new MatrixXd);
        double L = 1.0;             // The Lipschitz constant for third derivative
        // The approximation ratio, = 8/epsilon * (n^1.5) //
        // we select epsilon = 1, giving Q = 8 * n ^1.5
        double Q = 8.0 * pow(n,1.5);
        // need to decide a suitable approximation ratio and L for the last two arguments //
        double Cq = computeCompetetiveSubspace(H, T, S, Q, L);
        eps_1 = grad->norm();
        // need to decide L in the following
        if (Cq >= Q*pow(24*eps_1*1,1/3)) {
            // need to decide Q in the following
            auto u = approximateTensorNorm(T, S, Q);
            for (int j = 0; j < n; ++j)
                third_ord_min[j] = z.xnew[j] - Cq/(1*Q)*u->coeffRef(j);
        }
        else
            for (int j = 0; j < n; ++j)
                third_ord_min[j] = z.xnew[j];
    }
    // print out the results for debugging //
//    for (int i = 0; i < n; ++i)
//        cout << third_ord_min[i] << endl;
    
    vector<ReverseAD::adouble> x(n);
    for (int i = 0; i < n; ++i)
        x[i] <<= third_ord_min[i];
    adouble y = funct(x);
    cout << y.getVal() << endl;
    return third_ord_min;
}
