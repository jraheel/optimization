//
//  firstorderoptimization.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//


#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <iomanip>
#include <fstream>
#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseQR>
#include <gsl/gsl_multimin.h>

#include "tensors.hpp"
#include "objfunction.hpp"

#include "firstorderoptimization.hpp"

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::trace_on;
using ReverseAD::trace_off;

using namespace ReverseAD;

// the objective function evaluation
double comp_f(const gsl_vector *v,
              void *params)
{
    std::vector<adouble> x(v->size);
    for (int i = 0; i < v->size; ++i)
        x[i] <<= gsl_vector_get(v,i);
    return funct(x).getVal();
}


// computing the gradient, df, of f
void comp_df (const gsl_vector *v,
              void *params,
              gsl_vector *df)
{    
    std::vector<adouble> x(v->size);
    adouble y;
    double vy;
    trace_on<double>();                                             // begin tracing
    
    for (int i = 0; i < v->size; ++i)
        x[i] <<= gsl_vector_get(v,i);                               // setting the input variables
    y = funct(x);                                                   // function evaluation
    y >>= vy;                                                       // setting the output variable
    std::shared_ptr<TrivialTrace<double>> trace
                = trace_off<double>();                              // end tracing
    std::unique_ptr<BaseReverseHessian<double>> hessian
                            (new BaseReverseHessian<double>(trace));
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = hessian->compute(v->size,1);
    size_t size;
    size_t ** tind;
    double* values;
    tensor->get_internal_coordinate_list(0, 1, &size, &tind, &values);
    for (int i = 0; i < v->size; ++i)
        gsl_vector_set(df, i, 0);
    
    for (int i = 0; i < size; ++i)
        gsl_vector_set(df, tind[i][0], values[i]);
    
    // for debugging //
//    auto grad = computeGrad(tensor, v->size);
//    std::cout << *grad << std::endl;
}

// Compute both f and df together. //
void
comp_fdf (const gsl_vector *x,
          void *params,
          double *f, gsl_vector *df)
{
    *f = comp_f(x, params);
    comp_df(x, params, df);
}

//////////////////////////////////////////////////////////////////////////////////
/*                                                                              //
 Function polak_ribiere                                                         //
 A wrapper for Polak-Ribiere conjugate gradient method implemented by the       //
 GNU Scientific Library.                                                        //
                                                                                //
 Inputs:                                                                        //
 -  x     : A gsl_vector pointer, pointing to the indepedent variables          //
 -  my_fun: A gsl_multimin_function_fdf object containing all information (see  //
 GSL documentation for details)                                                 //
 -  init  : A double array containing initial values of independent variables   //
 Outputs:                                                                       //
 -  number of iterations it took                                                //
 */                                                                             //
//////////////////////////////////////////////////////////////////////////////////
std::pair<double, unsigned long> polak_ribiere(gsl_multimin_function_fdf my_func,
                                               const double* init,
                                               const double initStepSize,
                                               const double lineMinimization,
                                               const double tol,
                                               const int max_iters)
{
    size_t iter = 0;
    int status;
    
    const gsl_multimin_fdfminimizer_type *T;
    gsl_multimin_fdfminimizer *s;
    gsl_vector* x;
    x = gsl_vector_alloc (my_func.n);
    for (int i = 0; i < my_func.n; ++i)
        gsl_vector_set (x, i, init[i]);
    
    // choice of algorithms //
    //T = gsl_multimin_fdfminimizer_steepest_descent;               // steepest descent
    T = gsl_multimin_fdfminimizer_conjugate_pr;                     // Polak-Ribiere (conjugate gradient)
    //T = gsl_multimin_fdfminimizer_conjugate_fr;                   // conjugate gradient
    s = gsl_multimin_fdfminimizer_alloc (T, my_func.n);
    
    gsl_multimin_fdfminimizer_set (s, &my_func, x, initStepSize, lineMinimization);
    
    do
    {
        ++iter;
        status = gsl_multimin_fdfminimizer_iterate (s);
        if (status)
            break;
        
        status = gsl_multimin_test_gradient (s->gradient, tol);
        
        //if (status == GSL_SUCCESS)                                // minimum found
        //    printf ("Minimum found at:\n");
        // print out the current values of the independent variables, and function //
        /*std::string vals;
        for (int i = 0; i < my_func.n; ++i) {
            vals += std::to_string(gsl_vector_get(s->x,i));
            vals += " ";
        }
        std::cout   << iter
                    << ": "
                    << vals
                    << '\t' << "f: "
                    << s->f
                    << '\n';*/
        //printf("%zu:   %10.16f\n",iter,s->f);
    }
    while (status == GSL_CONTINUE && iter < max_iters);
    double final_val = s->f;
    gsl_multimin_fdfminimizer_free (s);
    gsl_vector_free (x);
    return std::make_pair(final_val, iter-1);
}

