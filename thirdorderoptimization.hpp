//
//  thirdorderoptimization.h
//  optim
//
//  Created by Javad Ahmed Raheel on 2/13/19.
//  Copyright © 2019 Javad Ahmed Raheel. All rights reserved.
//

#ifndef thirdorderoptimization_hpp
#define thirdorderoptimization_hpp

#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <stdio.h>
#include <iomanip>
#include <algorithm>
#include <chrono>

#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "tensors.hpp"
#include "cubic_regularization.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function computeCompetitiveSubspace
 Implements algorithm 4 from Anandkumar and Ge's paper to computes the competitive subspace
 Inputs:
 - H: pointer to Hessian of the function
 - T: pointer to the third derivative tensor of the function
 - S: pointer to the competitive subspace matrix. This shall be changed in place
 - Q: approximation ratio
 - L: Lipschitz bound on the third derivative
 Outputs:
 - the Frobenius norm of the projected T onto S. If the return value is zero, then S is set to
 NULL, signifying no competitive subspace exists
*/
////////////////////////////////////////////////////////////////////////////////////////////////
double computeCompetetiveSubspace(std::shared_ptr<Eigen::SparseMatrix<double>> H,
                                  std::shared_ptr<Eigen::Tensor<double,3>> T,
                                  std::shared_ptr<Eigen::MatrixXd> S,
                                  double Q,
                                  double L);



////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function approximateTensorNorm
 Implements algorithm 3 from Anandkumar and Ge's paper.
 Computes a unit vector,u, in a competitive subspace such that for a mode three tensor
    T, T(u,u,u) >= the Frobenius norm of the projection of T onto the subspace defined by S
 Inputs:
 - T: pointer to the third derivative tensor of a function
 - S: pointer to the competitive subspace matrix.
 - Q: a double which is a universal constant = 8/epsilon * (n^1.5)
 Outputs:
 - u: pointer to the unit vector such that T(u,u,u) >= Frobenius norm of T projected onto S
 */
////////////////////////////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::VectorXd>
        approximateTensorNorm(std::shared_ptr<Eigen::Tensor<double,3>> T,
                              std::shared_ptr<Eigen::MatrixXd> S,
                              double Q);


////////////////////////////////////////////////////////////////////////////////////////////////
/*
 Function computeThirdOrderMinimimum
 Implements algorithm 2 from Anandkumar and Ge's paper.
 Computes a third order minimimum of a given function
 Inputs:
 - n   : dimension of the function to be minimized f:R^n ---> R
 - init: pointer to the vector of initial initial guess
 Outputs:
 - third_ord_min: a vector of doubles, storing the values of independent variables
 */
////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<double> computeThirdOrderMinimum(int n, double * init);
#endif /* thirdorderoptimization_hpp */
