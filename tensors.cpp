//
//  tensors.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//
#include <algorithm>
#include "objfunction.hpp"
#include "tensors.hpp"
#include <reversead.hpp>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/CXX11/Tensor>

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::trace_on;
using ReverseAD::trace_off;

using namespace Eigen;
using namespace std;

//////////////////////////////////////////////////////////////////////////////
/*                                                                          //
 Function compute Trace                                                     //
 Computes the trace to be used in computing derivatives later               //
 Inputs:                                                                    //
 - p: a pointer to an array of doubles holding the value at which the       //
 trace is to be computed                                                    //
 - n: the number of dimensions of the function f:R^n --> R                  //
 Outputs:                                                                   //
 pointer to the trace at p                                                  //
 */                                                                         //
//////////////////////////////////////////////////////////////////////////////
std::shared_ptr<TrivialTrace<double>> computeTrace(double* p, int n)
{
    vector<adouble> x(n);
    adouble y;
    double vy;
    trace_on<double>(); // begin tracing
    for (int i = 0; i < n; ++i)
        x[i] <<= p[i];
    y = funct(x);
    y >>= vy; // dependent variable
    std::shared_ptr<TrivialTrace<double>> trace =
                                    trace_off<double>(); // end tracing
    return trace;
}

//////////////////////////////////////////////////////////////////////////////
/*                                                                          //
 Function computeGrad.                                                      //
 Assembles the gradient tensor as Eigen                                     //
 (sparse) vector to be used for optimization.                               //
 Inputs:                                                                    //
 - tensor: an std::shared_ptr pointing to a data structure of type          //
 type DerivativeTensor. It holds all the required information               //
 of the values and indices                                                  //
 - n     : number of indepedent variables                                   //
 Outputs:                                                                   //
 - grad: an std::shared_ptr<SparseVector<double>> pointing to               //
 containing the values of the computed gradient                             //
 */                                                                         //
//////////////////////////////////////////////////////////////////////////////
std::shared_ptr<SparseVector<double>>
    computeGrad(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
         int n)
{
    size_t size;                                // nnz entries
    size_t** tind;                              // indices of the nonzero entries
    double* values;                             // the nz values
    // assemble gradient //
    tensor->get_internal_coordinate_list(0, 1, &size, &tind, &values);
    std::shared_ptr<SparseVector<double>> grad(new SparseVector<double>(n));
    for (size_t i = 0; i < size; ++i)
        grad->coeffRef(tind[i][0]) += values[i];
    return grad;
}


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function computeHessian.                                               //
 Assembles the Hessian tensor as Eigen                                  //
 (sparse) matrix to be used for optimization.                           //
 Inputs:                                                                //
 - tensor: an std::shared_ptr pointing to a data structure of type      //
 type DerivativeTensor. It holds all the required information           //
 of the values and indices                                              //
 - n     : number of indepedent variables                               //
 Outputs:                                                               //
 - Hessian: std::shared_ptr<SparseMatrix<double>> pointing to the       //
 computed values of the double derivatives (the Hessian).               //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<SparseMatrix<double>>
    computeHessian(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                   int n)
{
    size_t size;                                // nnz entries
    size_t** tind;                              // indices of the nonzero entries
    double* values;                             // the nz values
    // assemble Hessian //
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tensor->get_internal_coordinate_list(0, 2, &size, &tind, &values);
    tripletList.reserve(size);
    for (size_t i = 0; i < size; i++) {
        tripletList.push_back(T(tind[i][0],tind[i][1],values[i]));
        if (tind[i][0] != tind[i][1])           // Hessian is symmetric in our case
            tripletList.push_back(T(tind[i][1],tind[i][0],values[i]));
    }
    std::shared_ptr<SparseMatrix<double>> Hessian(new SparseMatrix<double>(n,n));
    Hessian->setFromTriplets(tripletList.begin(), tripletList.end());
    /*
    // eigenvalues of the Hessian //
    std::shared_ptr<MatrixXd> dHess(new MatrixXd(*Hessian));
    EigenSolver<MatrixXd> es(*dHess);
    cout << es.eigenvalues() << endl << endl;
     */
    return Hessian;
}


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function computeThirdDerivative.                                       //
 Assembles the third derivatie tensor as Eigen Tensor                   //
 (sparse) matrix to be used for optimization.                           //
 Inputs:                                                                //
 - tensor: an std::shared_ptr pointing to a data structure of type      //
 DerivativeTensor. It holds all the required information                //
 of the values and indices                                              //
 - n     : number of indepedent variables                               //
 Outputs:                                                               //
 - Hessian: std::shared_ptr<SparseMatrix<double>> pointing to the       //
 computed values of the double derivatives (the Hessian).               //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::Tensor<double,3>>
    computeThirdDerivative(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                           int n)
{
    // retrieve results
    size_t size;
    size_t** tind;
    double* values;
    // third order deriative : dependent_variable_index[0].order[3]
    tensor->get_internal_coordinate_list(0, 3, &size, &tind, &values);
    std::shared_ptr<Eigen::Tensor<double,3>> thirdDerivative(new Eigen::Tensor<double,3>(n,n,n));
    thirdDerivative->setZero();                           // set all values to zero
    // fill the derivative tensor with appropriate values //
    for (size_t i = 0; i < size; i++) {
        // find all permutations of the indices //
        int indices[] = {(int)tind[i][0],(int)tind[i][1],(int)tind[i][2]};
        std::sort(indices,indices+3);
        do {
            (*thirdDerivative)(indices[0],indices[1],indices[2]) = values[i];
        } while (next_permutation(indices,indices+3));
    }
    //std::cout << *thirdDerivative << endl;
    /*
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                cout << i << "," << j << "," << k << ":" << (*thirdDerivative)(i,j,k) << endl;*/
    return thirdDerivative;
}


//////////////////////////////////////////////////////////////////////
/*                                                                  //
 Function computeJacobian.                                          //
 Assembles the Jacobian tensor as Eigen                             //
 (sparse) matrix to be used for optimization.                       //
 Inputs:                                                            //
 - tensor: an std::shared_ptr pointing to a data structure of type  //
 type DerivativeTensor. It holds all the required information       //
 of the values and indices                                          //
 - n     : number of indepedent variables                           //
 - m     : number of functions, i.e. F = (f1,f2,...,fm)             //
 Outputs:                                                           //
 - Jacobian: std::shared_ptr<SparseMatrix<double>> pointing to      //
 the matrix containing the computed Jacobian values                 //
 */                                                                 //
//////////////////////////////////////////////////////////////////////
std::shared_ptr<SparseMatrix<double>>
    computeJacobian(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                    int n,
                    int m)
{
    size_t size; // nnz entries
    size_t** tind; // indices of the nonzero entries
    double* values; // the nz values
    // assemble Jacobian //
    typedef Eigen::Triplet<double> T;
    std::vector<T> tripletList;
    tripletList.reserve(n*m);
    for (int j = 0; j < m; ++j) {
        // make the gradients of fi be the i-th row of Jacobian //
        tensor->get_internal_coordinate_list(j, 1, &size, &tind, &values);
        for (size_t i = 0; i < size; i++)
            tripletList.push_back(T(j, (int) tind[i][0], (double) values[i]));
    }
    std::shared_ptr<SparseMatrix<double>> Jacobian (new SparseMatrix<double> (m,n));
    Jacobian->setFromTriplets(tripletList.begin(), tripletList.end());
    return Jacobian;
}


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function projectTensor                                                 //
 Projects the rank three tensor onto the spase described by the         //
 projection matrix.                                                     //
 T[P,P,P]_{i,j,k} = \sum_{q,r,s = 1}^n P_{i,q}P_{j,r,}P_{k,s}C^{q,r,s}  //
 Inputs:                                                                //
 - T: an std::shared_ptr pointing to a data structure of type           //
 Eigen::Tensor of rank 3.                                               //
 - P: an std::shared_ptr pointing to an                                 //
 Eigen::SparseMatrix that is a projection matrix for a space            //
 Outputs:                                                               //
 - a pointer to the projected tensor                                    //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::Tensor<double,3>>
    projectTensor(std::shared_ptr<Eigen::Tensor<double,3>> T,
                  std::shared_ptr<Eigen::MatrixXd> P)
{
    int n = (int) T->dimension(0);
    std::shared_ptr<Eigen::Tensor<double,3>> projected_T(new Eigen::Tensor<double,3>(n,n,n));
    projected_T->setZero();
    for (int i = 0; i < P->rows(); ++i)
        for (int j = 0; j < P->rows(); ++j)
            for (int k = 0; k < P->rows(); ++k)
                for (int q = 0; q < P->rows(); ++q)
                    for (int r = 0; r < P->rows(); ++r)
                        for (int s = 0; s < P->rows(); ++s) {
                            (*projected_T)(i,j,k) = (*projected_T)(i,j,k) +
                                                    (*P)(i,q) *
                                                    (*P)(j,r) *
                                                    (*P)(k,s) *
                                                    (*T)(q,r,s);
                        }
    return projected_T;
}


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function frobeniusNorm                                                 //
 Computes the frobenius norm of a tensor of rank 3                      //
 Inputs:                                                                //
 - T: an std::shared_ptr pointing to a data structure of type           //
 Eigen::Tensor of rank 3.                                               //
 Outputs:                                                               //
 - the frobenius norm of the tensor                                     //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
double frobeniusNorm(std::shared_ptr<Eigen::Tensor<double,3>> T)
{
    double norm = 0;
    for (int i = 0; i < (int)(T->dimensions())[0]; ++i)
        for (int j = 0; j < (int)(T->dimensions())[0]; ++j)
            for (int k = 0; k < (int)(T->dimensions())[0]; ++k)
                norm += pow(((*T)(i,j,k)),2);
    return sqrt(norm);
}


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function tensorOfVector                                                //
 Computes T(u,u,u), for T a mode 3 tensor, and u a vector               //
 Inputs:                                                                //
 - T:  an std::shared_ptr pointing to a data structure of type          //
 Eigen::Tensor of mode 3                                                //
 - u: and std::shared_ptr pointing to an Eigen::VectorXd                //
 Outputs:                                                               //
 - a double representing T(u,u,u) = \sum_{i,j,k}T_{i,j,k}*u_i*u_j*u_k   //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
double tensorDot(std::shared_ptr<Eigen::Tensor<double,3>> T,
                 std::shared_ptr<Eigen::VectorXd> u)
{
    double Tu = 0.0;
    for (int i = 0; i < u->rows(); ++ i)
        for (int j = 0; j < u->rows(); ++ j)
            for (int k = 0; k < u->rows(); ++ k)
                Tu += (*T)(i,j,k) * (*u)(i) * (*u)(j) * (*u)(k);
    return Tu;
}
