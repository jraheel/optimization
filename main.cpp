//
//  main.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 6/17/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

//
//  main.cpp
//  revAD
//
//  Created by Nawabzada Javad Ahmed Raheel Khan Achakzai on 3/17/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <stdio.h>
#include <iomanip>
#include <algorithm>
#include <chrono>

#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseQR>
#include <gsl/gsl_multimin.h>

#include "tensors.hpp"
#include "objfunction.hpp"

#include "firstorderoptimization.hpp"
#include <IpIpoptApplication.hpp>
#include "nlp_problem.hpp"

#include "cubic_regularization.hpp"
#include "thirdorderoptimization.hpp"

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::BaseReverseAdjoint;
using ReverseAD::trace_on;
using ReverseAD::trace_off;


using namespace Eigen;
using namespace std;
using namespace Ipopt;
using namespace chrono;
using namespace ReverseAD;

const int MAX_ITERATIONS = 100;

template <typename T>
T f(const T& x1, const T& x2) {
    return sin(x1*x2);
    //return (pow(x1+1, 2) + pow(x1*x1 - x2, 2));
}

template <typename T>
T g(const T& x1,const T& x2) {
    //return pow(x,(double)1/3);
    //return (pow(x1+1, 2) + pow(x1*x1 - x2, 2));
    //return ((x1*x1) + (x2*x2));
    return pow(x1,2) + pow(x2,2);
}

template <typename T>
T foo(T x1, T x2) {
    return pow(x1, 2)/2 + x1*cos(x2);
}

template <typename T>
T* h(const T& x1, const T& x2) {
    T* F = new T[2];
    F[0] = 10.0*(x2 - pow(x1,2.0));
    F[1] = 1-x1;
    return F;
}

template <typename T>
T k(const T& x1, const T& x2, const T& x3, const T& x4) {
    return pow((x1 + 10.0*x2),2.0)
        + 5.0*pow((x3 - x4),2.0)
        + pow((x2-2.0*x3),4.0)
        + 10.0*pow((x1-x4),4.0);
}


//////////////////////////////////////////////////////////////////////

// to test assembleGradient() and assembleHessian() functions
void testGradHess(double* init, int n) {
    vector<adouble> x(n);
    adouble y;
    double vy;
    trace_on<double>(); // begin tracing
    for (int i = 0; i < n; ++i)
        x[i] <<= init[i];
    y = funct(x);
    y >>= vy; // dependent variable
    std::shared_ptr<TrivialTrace<double>> trace =
                        trace_off<double>(); // end tracing
    std::cout << "y = " << vy << std::endl;
    
    std::unique_ptr<ReverseAD::BaseReverseThird<double>> hessian
                                (new ReverseAD::BaseReverseThird<double>(trace));
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = hessian->compute(n,1);
    
    // retrieve results
    size_t size;
    size_t** tind;
    double* values;
    // adjoints : dep[0].order[1]
    tensor->get_internal_coordinate_list(0, 1, &size, &tind, &values);
    std::cout << "size of adjoints = " << size << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << "A["<< tind[i][0] << "] = " << values[i] << std::endl;
    }
    std::shared_ptr<SparseVector<double>> grad = computeGrad(tensor,n);
    cout << "gradient\n" << *grad << endl;
    // hessian : dep[0].order[2]
    tensor->get_internal_coordinate_list(0, 2, &size, &tind, &values);
    std::cout << "size of hessian = " << size << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << "H["<< tind[i][0] << ", " << tind[i][1]
        << "] = " << values[i] << std::endl;
    }
    std::shared_ptr<SparseMatrix<double>> Hessian = computeHessian(tensor,n);
    cout << "Hessian\n" << *Hessian << endl;
}

// a function to test the assembleJacobian()
void testJacobian() {
    adouble ax1,ax2,y,z;
    adouble* F;
    double vy, vz;
    ReverseAD::trace_on<double>(); // begin active region
    ax1 <<= 0.0; // ax1 is the first independent variable (index 0 in trace)
    ax2 <<= 0.0; // ax2 is second independent variable (index 1 in trace)
    F = h<adouble>(ax1,ax2);
    y = F[0]; z = F[1];
    y >>= vy; z >>= vz;
    std::shared_ptr<TrivialTrace<double>> trace = ReverseAD::trace_off<double>(); // end of active region
    ReverseAD::BaseReverseAdjoint<double> Adjoint(trace);
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = Adjoint.compute(2,2);
    size_t size;
    size_t** tind;
    double* values;
    // For the first dependent variable (index 0 in trace)
    tensor->get_internal_coordinate_list(0, 1, &size, &tind, &values);
    std::cout << "size of adjoints = " << size << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << "A["<< tind[i][0] << "] = " << values[i] << std::endl;
    }
    tensor->get_internal_coordinate_list(1, 1, &size, &tind, &values);
    std::cout << "size of adjoints = " << size << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << "A["<< tind[i][0] << "] = " << values[i] << std::endl;
    }
    std::shared_ptr<SparseMatrix<double>> Jacobian = computeJacobian(tensor, 2, 2);
    cout << *Jacobian << endl;
}


// a function to test third derivative //
void testThirdDerivative(double* init, int n)
{
    vector<adouble> x(n);
    adouble y;
    double vy;
    trace_on<double>(); // begin tracing
    for (int i = 0; i < n; ++i)
        x[i] <<= init[i];
    y = funct(x);
    y >>= vy; // dependent variable
    std::shared_ptr<TrivialTrace<double>> trace = trace_off<double>(); // end tracing
    std::cout << "y = " << vy << std::endl;
    
    std::unique_ptr<ReverseAD::BaseReverseThird<double>> thirdDer
                    (new ReverseAD::BaseReverseThird<double>(trace));
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = thirdDer->compute(n,1);
    
    std::shared_ptr<Eigen::Tensor<double,3>> thirdDerivativeTensor = computeThirdDerivative(tensor, n);
    // retrieve results
    size_t size;
    size_t** tind;
    double* values;
    // hessian : dep[0].order[3]
    tensor->get_internal_coordinate_list(0, 3, &size, &tind, &values);
    std::cout << "size of tensor = " << size << std::endl;
    for (size_t i = 0; i < size; i++) {
        std::cout << "T[" << tind[i][0] << ", "
                    << tind[i][1] << ", "
                    << tind[i][2] << "] = "
                    << values[i] << std::endl;
    }
}

// helper method to run the Polak-Ribiere algorithm //
pair<double,unsigned long> run_polak_ribiere(double init[],
                                             int m,
                                             double initStepSize,
                                             double lineMinimization,
                                             double tol,
                                             int max_iters)
{
    double* par = NULL;
    gsl_multimin_function_fdf my_func;
    
    my_func.n = m;
    my_func.f = comp_f;
    my_func.df = comp_df;
    my_func.fdf = comp_fdf;
    my_func.params = par;
    return polak_ribiere(my_func,
                  init,
                  initStepSize,
                  lineMinimization,
                  tol,
                  max_iters);
}

// helper routine to run Ipopt on the test problem //
pair<double,unsigned long> runIpoptTest(double* init,
                                        int n,
                                        double tol)
{
    // Create a new instance of your nlp
    //  (use a SmartPtr, not raw)
    //SmartPtr<NlpProblem> mynlp = new NlpProblem(init, n);
    SmartPtr<NlpProblem> mynlp = new NlpProblem(init, n);
    
    // Create a new instance of IpoptApplication
    //  (use a SmartPtr, not raw)
    // We are using the factory, since this allows us to compile this
    // example with an Ipopt Windows DLL
    SmartPtr<IpoptApplication> app = new IpoptApplication();
    app->RethrowNonIpoptException(true);
    
    // Change some options
    // Note: The following choices are only examples, they might not be
    //       suitable for your optimization problem.
    app->Options()->SetNumericValue("tol", tol);
    app->Options()->SetStringValue("mu_strategy", "adaptive");
    app->Options()->SetStringValue("linear_solver", "ma86");
    app->Options()->SetIntegerValue("max_iter", MAX_ITERATIONS);
    app->Options()->SetIntegerValue("print_level", 0);
    // The following overwrites the default name (ipopt.opt) of the
    // options file
    // app->Options()->SetStringValue("option_file_name", "hs071.opt");
    
    // Initialize the IpoptApplication and process the options
    ApplicationReturnStatus status;
    status = app->Initialize();
    
    status = app->Initialize();
    if (status != Solve_Succeeded) {
        printf("\n\n*** Error during initialization!\n");
        //return (int) status;
        return make_pair(0, -1);
    }
    
    //ask Ipopt to solve the problem
    status = app->OptimizeTNLP(mynlp);
    
    /*if (status == Solve_Succeeded) {
        std::cout << "\n\n*** The problem SOLVED!\n";
    }
    else {
        std::cout << "\n\n*** The problem FAILED!\n";
    }*/
    
    // As the SmartPtrs go out of scope, the reference count
    // will be decremented and the objects will automatically
    // be deleted.

    //return (int) status;
    return mynlp->get_nlp_stats();
}

Vector3d phi(double t, int settings) {
    Vector3d p;
    if (settings == 1) {
        if (t <= 25) {
            p[0] = 1.0;
            p[1] = 1.0;
            p[2] = 1.0;
        }
        else {    // t > 25
            p[0] = 2.0;
            p[1] = -1.0;
            p[2] = -2.0;
        }
    }
    else { // settings = 2
        p[0] = 1.0;
        p[1] = 1.0 + 3.0 * (double)sin(t);
        p[2] = 1.0 + 3.0 * (double)cos(t);
    }
    return p;
}

adouble* f(const vector<adouble>& x,
           const double t, const int settings) {
    // set the design parameters //
    int n = x.size();
    double gamma = 0.1;
    double beta = 1;
    double mu = 2*gamma/beta;
    Vector3d theta_star(1,-2, 5);   // the actual theta*
    Vector3d p = phi(t, settings);
    double N = 1 + mu*p.transpose()*p;
    Matrix3d X = gamma*p*p.transpose();
    Vector3d v = gamma*p*p.transpose()*theta_star;

    adouble* dy = new adouble[n];
    dy[0] = -X(0,0)*x[3] -X(0,1)*x[4] -X(0,2)*x[5] + v[0];
    dy[1] = -X(1,0)*x[3] -X(1,1)*x[4] - X(1,2)*x[5] + v[1];
    dy[2] = -X(2,0)*x[3] -X(2,1)*x[4] - X(2,2)*x[5] + v[2];
    dy[3] = -beta*N*(x[3] - x[0]);
    dy[4] = -beta*N*(x[4] - x[1]);
    dy[5] = -beta*N*(x[5] - x[2]);

    return dy;
}

VectorXd g(const VectorXd& y_k,
        VectorXd yp,
        const double t,
        const int settings,
        double h) {
    int n = y_k.size();
    vector<adouble> y_curr(n);
    for (int i = 0; i < n; ++i)
        y_curr[i] <<= y_k[i];

    adouble* y = f(y_curr, t, settings);

    VectorXd y_f(n);
    for (int i = 0; i < n; ++i)
        y_f[i] = h*y[i].getVal();

    return y_k - y_f - yp;
}

shared_ptr<TrivialTrace<double>> getTrace(const VectorXd& x0,
        int dim,
        double t,
        int setting) {
    vector<adouble> x(dim);
    adouble* F;
    adouble* y = new adouble[dim];
    double* vy = new double[dim];

    ReverseAD::trace_on<double>(); // begin active region
    for (int i = 0;  i < dim; ++i)
        x[i] <<= x0[i];

    F = f(x, t, setting);
    for (int i = 0; i < dim; ++i) {
        y[i] = F[i];
        y[i] >>= vy[i];
    }
    shared_ptr<TrivialTrace<double>> trace = trace_off<double>(); // end of active region
    delete[] F;
    delete[] y;
    delete[] vy;
    return trace;
}
void run_Newton(string filename, int setting) {
    int dim = 6;
    int end_time = 50;
    VectorXd y0(dim);
    // set the initial values //
    for (int i = 0; i < dim; ++i)
        y0[i] = 0;
    double tol = 1e-6;
    double h = 1e-2;
    vector<double> t((int)end_time/h + 1);
    for (int i = 1; i < t.size(); ++i)
        t[i] = i*h;

    VectorXd yp(dim);   // previous value of y, i.e. y(i-1)
    yp = y0;
    MatrixXd A(dim,dim);
    ofstream ofile;
    ofile.open(filename);
    for (int j = 0; j < dim; ++j)
        ofile << yp[j] << ",";
    ofile << "\n";
    for (int i = 1; i < t.size(); ++i) {
        VectorXd y_k(dim);
        y_k = yp;
        for (int k = 1; k < MAX_ITERATIONS; ++k) {
            shared_ptr<TrivialTrace<double>> init_trace = getTrace(y_k, dim, t[i], setting);
            BaseReverseAdjoint<double> Adjoint(init_trace);
            shared_ptr<DerivativeTensor<size_t, double>> tensor = Adjoint.compute(dim,dim);
            shared_ptr<SparseMatrix<double>> Jacobian = computeJacobian(tensor, dim, dim);
            A = MatrixXd::Identity(dim, dim) - h * (*Jacobian);
            VectorXd v = g(y_k, yp, t[i], setting, h);
            VectorXd p_k = A.fullPivHouseholderQr().solve(-v);

            if (p_k.norm() >= tol) {
                y_k = y_k + p_k;
            }
            else
                break;
        }
        yp = y_k;
        for (int j = 0; j < dim - 1; ++j)
            ofile << yp[j] << ",";
        ofile << yp[dim-1]<< "\n";
    }
    ofile.close();
}
using namespace std::chrono;
int main(int argc, char** argv)
{

    auto start = high_resolution_clock::now();
    run_Newton("/home/jd/Desktop/results1.txt", 2);
    auto stop = high_resolution_clock::now();
    auto duration = duration_cast<microseconds>(stop - start) * 1e-6;
    cout << duration.count() << endl;
    return 0;
}
