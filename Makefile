# CHANGEME: This should be the name of your executable
EXE = main

# CHANGEME: Here is the name of all object files corresponding to the source
#           code that you wrote in order to define the problem statement
OBJS = main.o \
	firstorderoptimization.o \
	nlp_problem.o \
	objfunction.o \
	tensors.o \
	cubic_regularization.o \
	thirdorderoptimization.o


# CHANGEME: Additional libraries
ADDLIBS = 	-L/usr/local/lib -lreversead \
			-L/usr/lib -lgsl -lgslcblas \
			-L/usr/lib -lgfortran #\
													#-lSystem \
													#-lquadmath \
													#-lm \
													#-ldl \
													#-framework Accelerate

# CHANGEME: Additional flags for compilation (e.g., include flags)
ADDINCFLAGS = -I/usr/local/include/reversead -I/usr/include/eigen3/

##########################################################################
#  Usually, you don't have to change anything below.  Note that if you   #
#  change certain compiler options, you might have to recompile Ipopt.   #
##########################################################################

# C++ Compiler command
CXX = g++

# C++ Compiler options
CXXFLAGS = -Ofast -w -pipe -std=c++17 -DNDEBUG -Wparentheses -Wreturn-type -Wcast-qual -Wall -Wpointer-arith -Wwrite-strings -Wconversion -Wno-unknown-pragmas -Wno-long-long   -DIPOPT_BUILD

# additional C++ Compiler options for linking
CXXLINKFLAGS = 

# Include directories (we use the CYGPATH_W variables to allow compilation with Windows compilers)
#INCL = `PKG_CONFIG_PATH=/Users/jd/Desktop/Ipopt-3.12.10/build/lib64/pkgconfig:/Users/jd/Desktop/Ipopt-3.12.10/build/lib/pkgconfig:/Users/jd/Desktop/Ipopt-3.12.10/build/share/pkgconfig:  --cflags ipopt` $(ADDINCFLAGS)
INCL = -I`$(CYGPATH_W) /usr/local/include/coin-or`  -I/usr/local/include/coin-or -DCOIN_USE_MUMPS_MPI_H  $(ADDINCFLAGS)

# Linker flags
#LIBS = `PKG_CONFIG_PATH=/Users/jd/Desktop/Ipopt-3.12.10/build/lib64/pkgconfig:/Users/jd/Desktop/Ipopt-3.12.10/build/lib/pkgconfig:/Users/jd/Desktop/Ipopt-3.12.10/build/share/pkgconfig:  --libs ipopt`
#LIBS = -link -libpath:`$(CYGPATH_W) /Users/jd/Desktop/Ipopt-3.12.10/build/lib` libipopt.lib  -L/Users/jd/Desktop/Ipopt-3.12.10/build/lib -lcoinmumps -framework Accelerate -L/usr/local/Cellar/gcc/8.1.0/lib/gcc/8/gcc/x86_64-apple-darwin17.5.0/8.1.0 -L/usr/local/Cellar/gcc/8.1.0/lib/gcc/8/gcc/x86_64-apple-darwin17.5.0/8.1.0/../../.. -lgfortran -lSystem -lquadmath -lm  -L/Users/jd/Desktop/Ipopt-3.12.10/build/lib -lcoinmetis   -L/Users/jd/Desktop/Ipopt-3.12.10/build/lib -lcoinhsl -framework Accelerate -framework Accelerate -L/usr/local/Cellar/gcc/8.1.0/lib/gcc/8/gcc/x86_64-apple-darwin17.5.0/8.1.0 -L/usr/local/Cellar/gcc/8.1.0/lib/gcc/8/gcc/x86_64-apple-darwin17.5.0/8.1.0/../../.. -lgfortran -lSystem -lquadmath -lm  -L/Users/jd/Desktop/Ipopt-3.12.10/build/lib -lcoinmetis  -framework Accelerate -framework Accelerate -lm  -ldl
LIBS = -L/usr/local/lib -lipopt -lcoinmumps -lcoinmetis -lcoinhsl 

# The following is necessary under cygwin, if native compilers are used
CYGPATH_W = echo

all: $(EXE)

.SUFFIXES: .cpp .c .o .obj

$(EXE): $(OBJS)
	bla=;\
	for file in $(OBJS); do bla="$$bla `$(CYGPATH_W) $$file`"; done; \
	$(CXX) $(CXXLINKFLAGS) $(CXXFLAGS) -o $@ $$bla $(ADDLIBS) $(LIBS)

clean:
	rm -rf $(EXE) $(OBJS) ipopt.out

.cpp.o:
	$(CXX) $(CXXFLAGS) $(INCL) -c -o $@ $<


.cpp.obj:
	$(CXX) $(CXXFLAGS) $(INCL) -c -o $@ `$(CYGPATH_W) '$<'`

