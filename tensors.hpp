//
//  tensors.hpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

#ifndef tensors_hpp
#define tensors_hpp

#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <unsupported/Eigen/CXX11/Tensor>

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::trace_on;
using ReverseAD::trace_off;

//////////////////////////////////////////////////////////////////////////////
/*                                                                          //
 Function compute Trace                                                     //
 Computes the trace to be used in computing derivatives later               //
 Inputs:                                                                    //
 - x: a pointer to an array of doubles holding the value at which the       //
        trace is to be computed                                             //
 - n: the number of dimensions of the function f:R^n --> R                  //
 Outputs:                                                                   //
 pointer to the trace at x                                                  //
*/                                                                          //
//////////////////////////////////////////////////////////////////////////////
std::shared_ptr<TrivialTrace<double>> computeTrace(double* p, int n);


//////////////////////////////////////////////////////////////////////////////
/*                                                                          //
 Function computeGrad.                                                      //
 Assembles the gradient tensor as Eigen                                     //
 (sparse) vector to be used for optimization.                               //
 Inputs:                                                                    //
 - tensor: an std::shared_ptr pointing to a data structure of type          //
 type DerivativeTensor. It holds all the required information               //
 of the values and indices                                                  //
 - n     : number of indepedent variables                                   //
 Outputs:                                                                   //
 - grad: an std::shared_ptr<SparseVector<double>> pointing to               //
 containing the values of the computed gradient                             //
 */                                                                         //
//////////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::SparseVector<double>>
    computeGrad(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                 int n);

//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function computeHessian.                                               //
 Assembles the Hessian tensor as Eigen                                  //
 (sparse) matrix to be used for optimization.                           //
 Inputs:                                                                //
 - tensor: an std::shared_ptr pointing to a data structure of type      //
 type DerivativeTensor. It holds all the required information           //
 of the values and indices                                              //
 - n     : number of indepedent variables                               //
 Outputs:                                                               //
 - Hessian: std::shared_ptr<SparseMatrix<double>> pointing to the       //
 computed values of the double derivatives (the Hessian).               //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::SparseMatrix<double>>
    computeHessian(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                   int n);

//////////////////////////////////////////////////////////////////////
/*                                                                  //
 Function computeJacobian.                                          //
 Assembles the Jacobian tensor as Eigen                             //
 (sparse) matrix to be used for optimization.                       //
 Inputs:                                                            //
 - tensor: an std::shared_ptr pointing to a data structure of type  //
 type DerivativeTensor. It holds all the required information       //
 of the values and indices                                          //
 - n     : number of indepedent variables                           //
 - m     : number of functions, i.e. F = (f1,f2,...,fm)             //
 Outputs:                                                           //
 - Jacobian: std::shared_ptr<SparseMatrix<double>> pointing to      //
 the matrix containing the computed Jacobian values                 //
 */                                                                 //
//////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::SparseMatrix<double>>
    computeJacobian(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
                 int n, int m);

//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function computeThirdDerivative.                                       //
 Assembles the third derivatie tensor as Eigen Tensor                   //
 (sparse) matrix to be used for optimization.                           //
 Inputs:                                                                //
 - tensor: an std::shared_ptr pointing to a data structure of type      //
    DerivativeTensor. It holds all the required information             //
 of the values and indices                                              //
 - n     : number of indepedent variables                               //
 Outputs:                                                               //
 - Hessian: std::shared_ptr<SparseMatrix<double>> pointing to the       //
    computed values of the double derivatives (the Hessian).            //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::Tensor<double,3>>
    computeThirdDerivative(std::shared_ptr<DerivativeTensor<size_t, double>> tensor,
               int n);

//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function projectTensor                                                 //
 Projects the rank three tensor onto the spase described by the         //
 projection matrix.                                                     //
 T[P,P,P]_{i,j,k} = \sum_{q,r,s = 1}^n P_{i,q}P_{j,r,}P_{k,s}C^{q,r,s}  //
 Inputs:                                                                //
 - T: an std::shared_ptr pointing to a data structure of type           //
 Eigen::Tensor of rank 3.                                               //
 - P: an std::shared_ptr pointing to an                                 //
 Eigen::SparseMatrix that is a projection matrix for a space            //
 Outputs:                                                               //
 - a pointer to the projected tensor                                    //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
std::shared_ptr<Eigen::Tensor<double,3>>
    projectTensor(std::shared_ptr<Eigen::Tensor<double,3>> T,
                   std::shared_ptr<Eigen::MatrixXd> P);


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function frobeniusNorm                                                 //
 Computes the frobenius norm of a tensor of rank 3                      //
 Inputs:                                                                //
 - T: an std::shared_ptr pointing to a data structure of type           //
 Eigen::Tensor of rank 3.                                               //
 Outputs:                                                               //
 - the frobenius norm of the tensor                                     //
 */                                                                     //
//////////////////////////////////////////////////////////////////////////
double frobeniusNorm(std::shared_ptr<Eigen::Tensor<double,3>> T);


//////////////////////////////////////////////////////////////////////////
/*                                                                      //
 Function tensorOfVector                                                //
 Computes T(u,u,u), for T a mode 3 tensor, and u a vector               //
 Inputs:                                                                //
 - T:  an std::shared_ptr pointing to a data structure of type          //
 Eigen::Tensor of mode 3                                                //
 - u: and std::shared_ptr pointing to an Eigen::VectorXd                //
 Outputs:                                                               //
 - a double representing T(u,u,u) = \sum_{i,j,k}T_{i,j,k}*u_i*u_j*u_k   //
 Eigen::Tensor of rank 3.                                               //
*/                                                                      //
//////////////////////////////////////////////////////////////////////////
double tensorDot(std::shared_ptr<Eigen::Tensor<double,3>> T,
                 std::shared_ptr<Eigen::VectorXd> u);
#endif /* tensors_hpp */
