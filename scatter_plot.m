function scatter_plot(data,f)
    close all;
    n   = table2array(data(:,1));
    prt = table2array(data(:,5));
    ipt = table2array(data(:,9));
    scatter(n,prt,20,'filled','blue');
    hold on
    scatter(n,ipt,20,'filled','red');
    xlabel('dimensions (#)','FontSize',20);
    ylabel('Time (seconds)','FontSize',20);
    title(f,'FontSize',20);
    legend('Polak-Ribiere','Interior-Point');
    grid on;
end