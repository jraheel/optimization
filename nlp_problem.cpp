
#include "nlp_problem.hpp"

#include <cassert>
#include <iostream>
#include <Eigen/Eigenvalues>
#include "tensors.hpp"
#include "objfunction.hpp"

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::trace_on;
using ReverseAD::trace_off;


using namespace Ipopt;
using namespace ReverseAD;

// returns the size of the problem
bool NlpProblem::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                             Index& nnz_h_lag, IndexStyleEnum& index_style)
{
    // The problem described in HS071_NLP.hpp has 4 variables, x[0] through x[3]
    n = numInds;
    
    // one equality constraint and one inequality constraint
    m = 0;
    
    // in this example the jacobian is dense and contains 8 nonzeros
    nnz_jac_g = 0;
    
    // the hessian is also dense and has 16 total nonzeros, but we
    // only need the lower left corner (since it is symmetric)
    nnz_h_lag = n*(n+1)/2;
    
    // get the function trace //
    generate_traces(n, m, nnz_jac_g, nnz_h_lag);
    // use the C style indexing (0-based)
    index_style = TNLP::C_STYLE;
    return true;
}

// returns the variable bounds
bool NlpProblem::get_bounds_info(Index n, Number* x_l, Number* x_u,
                                Index m, Number* g_l, Number* g_u)
{
    // here, the n and m we gave IPOPT in get_nlp_info are passed back to us.
    // If desired, we could assert to make sure they are what we think they are.
    assert(n == numInds);
    assert(m == 0);
    
    // the variables have lower bounds of 1
    for (Index i=0; i < n; ++i) {
        x_l[i] = -2e19; // no lower bound
    }
    
    // the variables have upper bounds of 5
    for (Index i=0; i < n; ++i) {
        x_u[i] = 2e19; // no upper bound
    }
    
    // the first constraint g1 has a lower bound of 25
    //g_l[0] = 25;
    // the first constraint g1 has NO upper bound, here we set it to 2e19.
    // Ipopt interprets any number greater than nlp_upper_bound_inf as
    // infinity. The default value of nlp_upper_bound_inf and nlp_lower_bound_inf
    // is 1e19 and can be changed through ipopt options.
    //g_u[0] = 2e19;
    
    // the second constraint g2 is an equality constraint, so we set the
    // upper and lower bound to the same value
    //g_l[1] = g_u[1] = 40.0;
    return true;
}

// returns the initial point for the problem
bool NlpProblem::get_starting_point(Index n, bool init_x, Number* x,
                                   bool init_z, Number* z_L, Number* z_U,
                                   Index m, bool init_lambda,
                                   Number* lambda)
{
    // Here, we assume we only have starting values for x, if you code
    // your own NLP, you can provide starting values for the dual variables
    // if you wish
    assert(init_x == true);
    assert(init_z == false);
    assert(init_lambda == false);
    
    // initialize to the given starting point
    for (int i = 0; i < n; ++i)
        x[i] = initVals[i];
    return true;
}

// returns the value of the objective function
bool NlpProblem::eval_f(Index n, const Number* x, bool new_x, Number& obj_value)
{
    assert(n == numInds);
    
    std::vector<adouble> indep_vars(n);
    for (int i = 0; i < n; ++i)                             // setting values of independent variables
        indep_vars[i] <<= x[i];
    
    obj_value = funct(indep_vars).getVal();
    return true;
}

// return the gradient of the objective function grad_{x} f(x)
bool NlpProblem::eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f)
{
    assert(n == numInds);
    std::shared_ptr<TrivialTrace<double>> new_trace =
                    BaseFunctionReplay::replay_forward(trace_f, x, n);
    BaseReverseAdjoint<double> adjoint(new_trace);
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = adjoint.compute(n,1);
    size_t size;
    size_t ** tind;
    double* values;
    tensor->get_internal_coordinate_list(0, 1, &size, &tind, &values);
    for (int i = 0; i < n; ++i)
        grad_f[i] = 0;
    for (size_t i = 0; i < size; ++i)
        grad_f[tind[i][0]] = values[i];
    /*std::shared_ptr<Eigen::SparseVector<double>> gr = computeGrad(tensor,n);
    for (int i = 0; i < n; ++i)
        grad_f[i] = gr->coeffRef(i);*/
    /*
    std::vector<adouble> inds(n);
    adouble y;
    double vy;
    trace_on<double>();                                     // begin tracing
    for (int i = 0; i < n; ++i)                             // setting values of independent variables
        inds[i] <<= x[i];
    
    y = funct(inds);                                        // compute the function value
    y >>= vy;                                               // setting up the dependent variable
    std::shared_ptr<TrivialTrace<double>> trace =
        trace_off<double>();                                // end tracing
    std::unique_ptr<BaseReverseHessian<double>> hessian     // to compute the gradient
            (new BaseReverseHessian<double>(trace));
    std::shared_ptr<DerivativeTensor<size_t, double>> tensor = hessian->compute(n,1);
    std::shared_ptr<Eigen::SparseVector<double>> grad = computeGrad(tensor,n);
    for (int i = 0; i < n; ++i)
        grad_f[i] = grad->coeffRef(i);                      // set the gradient values
     */
    return true;
}

// return the value of the constraints: g(x)
bool NlpProblem::eval_g(Index n, const Number* x, bool new_x, Index m, Number* g)
{
    assert(n == numInds);
    assert(m == 0);
    // set the Jacobian values for the constraints here //
    //g[0] = 0;
    //g[1] = 0;
    return true;
}

// return the structure or values of the jacobian
bool NlpProblem::eval_jac_g(Index n, const Number* x, bool new_x,
                           Index m, Index nele_jac, Index* iRow, Index *jCol,
                           Number* values)
{ /*
   if (values == NULL) {
   // return the structure of the jacobian
   
   // this particular jacobian is dense
   iRow[0] = 0;
   jCol[0] = 0;
   iRow[1] = 0;
   jCol[1] = 1;
   iRow[2] = 0;
   jCol[2] = 2;
   iRow[3] = 0;
   jCol[3] = 3;
   iRow[4] = 1;
   jCol[4] = 0;
   iRow[5] = 1;
   jCol[5] = 1;
   iRow[6] = 1;
   jCol[6] = 2;
   iRow[7] = 1;
   jCol[7] = 3;
   }
   else {
   // return the values of the jacobian of the constraints
   values[0] = x[1]*x[2]*x[3]; // 0,0
   values[1] = x[0]*x[2]*x[3]; // 0,1
   values[2] = x[0]*x[1]*x[3]; // 0,2
   values[3] = x[0]*x[1]*x[2]; // 0,3
   
   values[4] = 2*x[0]; // 1,0
   values[5] = 2*x[1]; // 1,1
   values[6] = 2*x[2]; // 1,2
   values[7] = 2*x[3]; // 1,3
   }
   */
    return true;
}

//return the structure or values of the hessian
bool NlpProblem::eval_h(Index n, const Number* x, bool new_x,
                       Number obj_factor, Index m, const Number* lambda,
                       bool new_lambda, Index nele_hess, Index* iRow,
                       Index* jCol, Number* values)
{
    if (values == NULL) {
        // return the structure. This is a symmetric matrix, fill the lower left
        // triangle only.
        
        // the hessian for this problem is actually dense
        Index idx=0;
        for (Index row = 0; row < n; row++) {
            for (Index col = 0; col <= row; col++) {
                iRow[idx] = row;
                jCol[idx] = col;
                idx++;
            }
        }
        
        assert(idx == nele_hess);
    }
    else {
        std::shared_ptr<TrivialTrace<double>> new_trace =
                BaseFunctionReplay::replay_forward(trace_f, x, n);
        BaseReverseHessian<double> hessian(new_trace);
        std::shared_ptr<DerivativeTensor<size_t, double>> tensor = hessian.compute(n,1);
        size_t size;                                // nnz entries
        size_t** tind;                              // indices of the nonzero entries
        double* vals;                               // the nz values
        tensor->get_internal_coordinate_list(0, 2, &size, &tind, &vals);
        for (int i = 0; i < (int)n*(n+1)/2; ++i)
            values[i] = 0;
        
        for (size_t i = 0; i < size; ++i){
            size_t r = tind[i][0], c = tind[i][1];
            size_t k = (size_t)r*(r+1)/2 + c;
            values[k] = vals[i];
        }
        
        //computeHessian(tensor,n);
        
        /*
        std::vector<adouble> inds(n);
        adouble y;
        double vy;
        trace_on<double>();                                     // begin tracing
        for (int i = 0; i < n; ++i)                             // setting values of independent variables
            inds[i] <<= x[i];
        
        y = funct(inds);                               // compute the function value
        y >>= vy;                                               // setting up the dependent variable
        std::shared_ptr<TrivialTrace<double>> trace =
        trace_off<double>();                                    // end tracing
        std::unique_ptr<BaseReverseHessian<double>> hessian     // to compute the gradient
        (new BaseReverseHessian<double>(trace));
        std::shared_ptr<DerivativeTensor<size_t, double>> tensor = hessian->compute(n,1);
         
        // get the derivatives of order 1 for the first indepedent variable
        std::shared_ptr<Eigen::SparseMatrix<double>> H = computeHessian(tensor,n);
        
        // return the values. This is a symmetric matrix, fill the lower
        // triangle only
        int k = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j <=i; ++j)
                values[k++] = H->coeffRef(i, j);
        }*/
    }
    return true;
}

void NlpProblem::finalize_solution(SolverReturn status,
                                  Index n, const Number* x, const Number* z_L, const Number* z_U,
                                  Index m, const Number* g, const Number* lambda,
                                  Number obj_value,
                                  const IpoptData* ip_data,
                                  IpoptCalculatedQuantities* ip_cq)
{
    // here is where we would store the solution to variables, or write to a file, etc
    // so we could use the solution.
    //std::cout << "\n\nTotal Iterations: " << ip_data->iter_count()+1 << std::endl;
    totIters = ip_data->iter_count() + 1;
    finalVal = obj_value;
    // For this example, we write the solution to the console
    //std::cout << std::endl << std::endl << "Solution of the primal variables, x" << std::endl;
    /*for (Index i=0; i<n; i++) {
        std::cout << "x[" << i << "] = " << x[i] << std::endl;
    }*/
    /*
    std::cout << std::endl << std::endl << "Solution of the bound multipliers, z_L and z_U" << std::endl;
    for (Index i=0; i<n; i++) {
        std::cout << "z_L[" << i << "] = " << z_L[i] << std::endl;
    }
    for (Index i=0; i<n; i++) {
        std::cout << "z_U[" << i << "] = " << z_U[i] << std::endl;
    }
    */
    //std::cout << std::endl << std::endl << "Objective value" << std::endl;
    //std::cout << "f(x*) = " << obj_value << std::endl;
   /*
    std::cout << std::endl << "Final value of the constraints:" << std::endl;
    for (Index i=0; i<m ;i++) {
        std::cout << "g(" << i << ") = " << g[i] << std::endl;
    }*/
}

//////////////////////////////////////////////////////////
// To generate the traces which would be used later.    //
//////////////////////////////////////////////////////////
void NlpProblem::generate_traces(Index n, Index m, Index& nnz_jac_g, Index& nnz_h_lag)
{
    
   // Number *xp    = new double[n];
    //Number *lamp  = new double[m];
    //Number *zl    = new double[m];
    //Number *zu    = new double[m];
    
    //adouble *xa   = new adouble[n];
    //adouble *g    = new adouble[m];
    //double *lam   = new double[m];
    //double sig;
    adouble obj_value;
    double dummy;
    
    //int i,j,k,l,ii;
    //tmp_x = new double[n];
    //obj_lam   = new double[m+1];
    
    //get_starting_point(n, true, xp, 0, zl, zu, m, 0, lamp);
    
    // get the trace for function value //
    std::vector<adouble> indepVars(n);
    trace_on<double>();
    
    for(Index idx=0; idx<n; ++idx)
        indepVars[idx] <<= initVals[idx];
    obj_value = funct(indepVars);
    
    obj_value >>= dummy;
    
    trace_f = trace_off<double>();
    
    /*
    // get the trace for constraints //
    trace_on<double>();
    for(Index idx=0;idx<n;idx++)
        xa[idx] <<= xp[idx];
    
    eval_constraints(n,xa,m,g);
    
    
    for(Index idx=0;idx<m;idx++)
        g[idx] >>= dummy;
    
    trace_g = trace_off<double>();
    
    // get the trace for hessians //
    trace_on<double>();

    for(Index idx=0;idx<n;idx++)
        indepVars[idx] <<= initVals[idx];
    for(Index idx=0;idx<m;idx++)
        lam[idx] = 1.0;
    sig = 1.0;
    
    obj_value = funct(indepVars);
    
    obj_value = obj_value * adouble::markParam(sig);
    eval_constraints(n,xa,m,g);
    
    for(Index idx=0;idx<m;idx++)
        obj_value = obj_value + g[idx]*adouble::markParam(lam[idx]);
    
    obj_value >>= dummy;
    
    trace_L = trace_off<double>();
    
    rind_g = NULL;
    cind_g = NULL;
    rind_L = NULL;
    cind_L = NULL;
    jacval = NULL;
    hessval = NULL;
    
    BaseReverseAdjoint<double> adjoint(trace_g);
    adjoint.compute(n, m);
    adjoint.retrieve_adjoint_sparse_format(&nnz_jac, &rind_g, &cind_g, &jacval);
    
    nnz_jac_g = nnz_jac;
#ifdef SHOW_DEBUG_INFO
    for (int i = 0; i < nnz_jac; i++) {
        std::cout << "A[" << rind_g[i] << "," << cind_g[i] << " ] = "
        << jacval[i] << std::endl;
    }
#endif
    BaseReverseHessian<double> hessian(trace_L);
    hessian.compute(n, 1);
    hessian.retrieve_hessian_sparse_format(&nnz_L, &rind_L, &cind_L, &hessval);
    
#ifdef SHOW_DEBUG_INFO
    for (int i = 0; i < nnz_L[0]; i++) {
        std::cout << "H[" << rind_L[0][i] << "," << cind_L[0][i] << " ] = "
        << hessval[0][i] << std::endl;
    }
#endif
    
    nnz_h_lag = (n*(n+1)) / 2;
    
    delete[] lam;
    delete[] g;
    delete[] xa;
    delete[] zu;
    delete[] zl;
    delete[] lamp;
    delete[] xp;
    */
}
