function dydt = odefun(t,y)
    gamma = 0.1;
    beta = 1;
    mu = 2*gamma/beta;
    N = (1 + mu*3);
    dydt = zeros(6,1);
    dydt(1) = -gamma*(y(4) + y(5) + y(6) - 4);
    dydt(2) = -gamma*(y(4) + y(5) + y(6) - 4);
    dydt(3) = -gamma*(y(4) + y(5) + y(6) - 4);
    dydt(4) = -beta*(y(4) - y(1));
    dydt(5) = -beta*(y(5) - y(2));
    dydt(6) = -beta*(y(6) - y(3));
end

