//
//  objfunction.hpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

#ifndef objfunction_hpp
#define objfunction_hpp

#include <reversead.hpp>

ReverseAD::adouble funct(const std::vector<ReverseAD::adouble>& x);

ReverseAD::adouble* vector_function(const std::vector<ReverseAD::adouble>& x, int n);

#endif /* objfunction_hpp */
