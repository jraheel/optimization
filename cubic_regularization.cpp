//
//  cubic_regularization.cpp
//  optim
//
//  Created by Javad Ahmed Raheel on 10/31/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//
#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <typeinfo>
#include <Eigen/Eigenvalues>
#include <Eigen/Dense>
#include <Eigen/SparseCholesky>
#include <Eigen/QR>
#include <Eigen/Dense>
#include <complex>
#include <gsl/gsl_poly.h>

#include "tensors.hpp"
#include "objfunction.hpp"
#include "cubic_regularization.hpp"

using ReverseAD::adouble;
using ReverseAD::TrivialTrace;
using ReverseAD::BaseReverseHessian;
using ReverseAD::DerivativeTensor;
using ReverseAD::trace_on;
using ReverseAD::trace_off;

using namespace ReverseAD;
using namespace std;
using namespace Eigen;

const unsigned long MAXITERS    = 10000;        // max allowed iterations
const double eps                = 1.4901e-08;   // sqrt(machine epsilon)

bool hasConverged(const VectorXd &s,
                  const double lambduh,
                  const double M,
                  const double kappa)
{
    double r = 2*lambduh/M;
    return (abs(s.norm() - r) <= kappa);
}

double update_lambduh(const double lambduh,
                      const VectorXd &s,
                      const MatrixXd& L,
                      const double M)
{
    VectorXd w = L.colPivHouseholderQr().solve(s);
    double norm_s = s.norm();
    double phi = 1/norm_s - M/(2*lambduh);
    double phi_prime = w.norm()*w.norm()/(pow(norm_s,3)) +
                        M/(2*lambduh*lambduh);
    return lambduh - phi/phi_prime;
}

// constructor
CubicRegularization::CubicRegularization(double* initVals,
                                         int dim,
                                         double L,
                                         double L0,
                                         double kappa)
{
    double dummy;           // a dummy double variable to signify output variable status to f
    n = dim;
    this->L = L;
    this->L0 = L0;
    Mk = L;
    this->kappa = kappa;
    iters = 0;
    xold = new double[n];
    xnew = new double[n];
    this->s_success = false;
    // set the trace for derivaties//
    std::vector<adouble> indepVars(n);
    trace_on<double>();
    for(int i=0; i < n; ++i) {
        indepVars[i] <<= initVals[i];
        xold[i] = initVals[i];
        xnew[i] = initVals[i];
    }
    f = funct(indepVars);   // compute objective function
    f >>= dummy;
    trace = trace_off<double>();
    evalDerivatives();
    compute_lambda_nplus();
    tolerance = 1e-9;
}

void CubicRegularization::evalDerivatives()
{
    std::shared_ptr<TrivialTrace<double>> new_trace =
        BaseFunctionReplay::replay_forward(trace, xnew, n);
    std::unique_ptr<BaseReverseHessian<double>>
        hessian(new BaseReverseHessian<double>(new_trace));
    std::shared_ptr<DerivativeTensor<size_t, double>>
                            tensor = hessian->compute(n,1);
    grad    = computeGrad(tensor,n);
    Hessian = computeHessian(tensor, n);
}

void CubicRegularization::computeCubicRegularization()
{
    bool converged = false;
    //xnew = x;
    Mk = L0;
    while (iters < MAXITERS && !converged) {
        for (int i = 0; i < n; ++i)
            xold[i] = xnew[i];
        compute_x_new();
        evalDerivatives();
        compute_lambda_nplus();
        converged = max(sqrt(2/(L+Mk))*grad->norm(), -2/(2*L + Mk)*lambda_n) <= tolerance;
        ++iters;
    }
    //cout << "Cubic Regularization Done!" << endl;
    vector<adouble> finalIndepVars(n);
    for (int i = 0; i < n; ++i) {
        //cout << xnew[i] << endl;
        finalIndepVars[i] <<= xnew[i];
    }
    f = funct(finalIndepVars);
    //cout << f.getVal() << endl;
}

void CubicRegularization::compute_x_new()
{
    if (L >= 0) {
        auto s = computeStep();
        for (int i = 0; i < n; ++i)
            xnew[i] = xold[i] + s[i];
    }
    else {
        bool decreased = false;
        int iter = 0;
        double y = f.getVal();
        while (!decreased && iter < MAXITERS) {
            Mk *= 2;
            auto s = computeStep();
            for (int i = 0; i < n; ++i)
                xnew[i] = xold[i] + s[i];
            std::vector<adouble> indepVars(n);
            for(int i=0; i < n; ++i) {
                indepVars[i] <<= xnew[i];
            }
            f = funct(indepVars);
            decreased = (f.getVal() - y) <= 0;
            ++iter;
            if (iter == MAXITERS)
                cout << "Maximum allowed iteration reached. Could not compute cubic \
                        upper approximation\n";
            Mk = max(0.5*Mk,L0);
        }
    }
}

/*
Computes s in H_lambda*s = -g
param lambduh: value for lambda in H_lambda
return: s
*/
VectorXd CubicRegularization::compute_s(double lambduh)
{
    //auto H_lambda = *Hessian + MatrixXd::Identity(n,n)*lambduh;
    //cout << H_lambda << endl;
    // try to solve using Cholesky decomposition of H_lambda //
    SimplicialLLT<SparseMatrix<double>> solver;
    solver.compute(*Hessian + MatrixXd::Identity(n,n)*lambduh);
    if (solver.info() != Success) {     // Cholesky decomposition failed. Try with modified lambduh
        cout << "Cholesky decomposition failed. Trying with modified lambduh\n";
        lambda_const *= 2;
        lambduh = lambda_nplus + lambda_const;
        solver.compute(*Hessian + MatrixXd::Identity(n,n)*lambduh);
    }
    VectorXd s = solver.solve(-(*grad));
    if (solver.info() != Success) {
        cout << "Could not solve the systen H_lambda * s = -grad";
        s_success = false;
    }
    else
        s_success = true;
    return s;
}

/*
Solve the cubic regularization subproblem. See algorithm 7.3.6 in Conn et al. (2000).
:return: s: Step for the cubic regularization algorithm
 */
VectorXd CubicRegularization::computeStep()
{
    //evalDerivatives();              // update the derivatives at the current value
    double lambduh = 0;
    //cout << lambda_nplus << " , " << lambda_const << endl;
    if (lambda_nplus == 0)
        lambduh = 0;
    else
        lambduh = lambda_nplus + lambda_const;
    VectorXd s = compute_s(lambduh);
    
    if (!s_success) {           // Cholesky factorization failed: retry
        cout << "H_lambda * s = -grad failed to compute" << endl;
        s = VectorXd(n);
        for (int i = 0; i < n; ++i)
            s[i] = 0;
        return s;
    }
    
    double r = 2*lambduh/Mk;
    if (s.norm() <= r) {
        if (lambduh == 0 || s.norm() == r)
            return s;
        else {
            SelfAdjointEigenSolver<MatrixXd> es;
            es.compute(MatrixXd(*Hessian + MatrixXd::Identity(n,n)*lambda_nplus));
            auto Lambda = es.eigenvalues();
            auto U      = es.eigenvectors();
            auto D      = Lambda.asDiagonal();
            auto s_cri  = -U.transpose() *
                        MatrixXd(D).completeOrthogonalDecomposition().pseudoInverse() * U *
                        (*grad);
            double coeffs[3] = {s_cri.dot(s_cri)-4*lambda_nplus*lambda_nplus/(Mk*Mk),
                2*U.col(0).dot(s_cri),
                U.col(0).dot(U.col(0))};
            double roots[4];
            complex<double> comp_roots[2];
            gsl_poly_complex_workspace * w = gsl_poly_complex_workspace_alloc(3);
            gsl_poly_complex_solve(coeffs,3,w,roots);
            gsl_poly_complex_workspace_free(w);
            for (int i = 0; i < 2; ++i)
                comp_roots[i] = complex<double>(roots[2*i],roots[2*i+1]);
            double alpha = real((norm(comp_roots[0]) > norm(comp_roots[1])) ?
                              comp_roots[0] : comp_roots[1]);
            s_success = true;
            return s_cri + U.col(0)*alpha;
        }
    }
    if (lambduh == 0)
        lambduh += lambda_const;
    LLT<MatrixXd> LLt(*Hessian + MatrixXd::Identity(n,n)*lambduh);
    MatrixXd chol_L = LLt.matrixL();
    unsigned long iter = 0;
    lambduh = update_lambduh(lambduh, s, chol_L, Mk);
    while ((iter < MAXITERS) &&
           !hasConverged(s,lambduh,Mk,kappa)) {
        iter += 1;
        lambduh = update_lambduh(lambduh, s, chol_L, Mk);
        auto s = compute_s(lambduh);
        if (s_success) // cholesky factorization successful
                return s;
        if (iter == MAXITERS){
            cout << "Warning: COuld not compute s in the allowed iterations\n";
            s_success = false;
        }
    }
    return s;
}

void CubicRegularization::compute_lambda_nplus()
{
    SelfAdjointEigenSolver<MatrixXd> es;
    es.compute(MatrixXd(*Hessian));
    lambda_n = es.eigenvalues()[0];             // smallest eigenvalue
    lambda_nplus = fmax(0,-es.eigenvalues()[0]);
    lambda_const = (1 + lambda_nplus) * eps;
}
