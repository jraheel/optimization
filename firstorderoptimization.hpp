//
//  firstorderoptimization.hpp
//  optim
//
//  Created by Javad Ahmed Raheel on 7/1/18.
//  Copyright © 2018 Javad Ahmed Raheel. All rights reserved.
//

#ifndef firstorderoptimization_hpp
#define firstorderoptimization_hpp
#include <memory>
#include <iostream>
#include <cassert>
#include <math.h>
#include <iomanip>
#include <fstream>
#include <reversead.hpp>

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SparseQR>
#include <gsl/gsl_multimin.h>

#include "tensors.hpp"
#include "objfunction.hpp"

// the objective function evaluation
double comp_f(const gsl_vector *v,
             void *params);

// computing the gradient, df, of f
void comp_df (const gsl_vector *v,
            void *params,
            gsl_vector *df);

/* Compute both f and df together. */
void
comp_fdf (const gsl_vector *x, void *params,
        double *f, gsl_vector *df);

/* A wrapper for Polak-Ribiere conjugate gradient method implemented by the
 GNU Scientific Library*/

std::pair<double, unsigned long> polak_ribiere(gsl_multimin_function_fdf my_func,
                                               const double* init,
                                               const double initStepSize,
                                               const double lineMinimization,
                                               const double tol,
                                               const int max_iters);
#endif /* firstorderoptimization_hpp */
