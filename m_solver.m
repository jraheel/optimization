function m_solver()
    gamma = 0.1;
    beta = 1;
    mu = 2*gamma/beta;
    setting = 2;
    theta_star = [1 -2 5]';
    Z = zeros(3);
    X = @(t) gamma*phi(t, setting)*phi(t, setting)';
    N = @(t)1 + mu*phi(t, setting)'*phi(t, setting);
    B = @(t)beta*N(t)*eye(3);
    A = @(t)[Z -X(t);B(t) -B(t)];
    F = @(t)[gamma*phi(t, setting)*phi(t, setting)'*theta_star; zeros(3,1)];
    h = 1e-3;
    dydt = @(t,y) A(t)*y + F(t);
    [t,xa] = ode45(dydt,[0,50],[0 0,0,0,0,0]');
    close all
    plot(t,xa(:,4),'-.r*','MarkerSize',1);
    hold on
    plot(t,xa(:,5),'--mo','MarkerSize',0.5);
    plot(t,xa(:,6),':bs','MarkerSize',0.05);
    plot(t, theta_star(1)*ones(size(t,1),1))
    plot(t, theta_star(2)*ones(size(t,1),1))
    plot(t, theta_star(3)*ones(size(t,1),1))
    xlabel('time')
    ylabel('theta')
    ylim([-4 6.5])
    legend('theta(1)', 'theta(2)', 'theta(3)', 'theta*(1)', 'theta*(2)', 'theta*(3)')
end

