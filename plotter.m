function plotter(y)
    h = 1e-2;
    t = 0:h:50;
    theta_star = [1 -2 5]';
    theta = y(4:6,:);
    close all
    plot(t,theta(1,:),'-.r*','MarkerSize',1);
    hold on
    plot(t,theta(2,:),'--mo','MarkerSize',0.5);
    plot(t,theta(3,:),':bs','MarkerSize',0.05);
    plot(t, theta_star(1)*ones(size(t,2),1))
    plot(t, theta_star(2)*ones(size(t,2),1))
    plot(t, theta_star(3)*ones(size(t,2),1))
    xlabel('time')
    ylabel('theta')
    ylim([-3 6])
    legend('theta(1)', 'theta(2)', 'theta(3)', 'theta*(1)', 'theta*(2)', 'theta*(3)')
end

