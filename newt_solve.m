function y = newt_solve()
    y0 = zeros(6,1);
    theta_star = [1 -2 5]';
    h = 1e-2;
    y = y0;
    g = @(y,y_p, p) y - h*f(1,y, p) - y_p;
    I = eye(6);
    t = 0:h:50;
    tol = 1e-6;
    setting = 2;
    max_iter = 1000;
    for i = 2:length(t)
        y_k = y(:,i-1);
        for k = 1:max_iter
            A = eye(6) - h*jacob(y_k, t(i), setting);
            j = jacob(y_k, t(i), setting);
            v = g(y_k,y(:,i-1), phi(t(i), setting));
            pk = A\(-g(y_k,y(:,i-1), phi(t(i), setting)));
            n = norm(A*pk + v);
            if norm(pk) >= tol
                y_k = y_k + pk;
            else
                break
            end
        end
        y(:,i) = y_k;
    end
    theta = y(4:6,:);
    close all
    plot(t,theta(1,:),'-.r*','MarkerSize',1);
    hold on
    plot(t,theta(2,:),'--mo','MarkerSize',0.5);
    plot(t,theta(3,:),':bs','MarkerSize',0.05);
    plot(t, theta_star(1)*ones(size(t,2),1))
    plot(t, theta_star(2)*ones(size(t,2),1))
    plot(t, theta_star(3)*ones(size(t,2),1))
    xlabel('time')
    ylabel('theta')
    ylim([-4 6.5])
    legend('theta(1)', 'theta(2)', 'theta(3)', 'theta*(1)', 'theta*(2)', 'theta*(3)')
end

